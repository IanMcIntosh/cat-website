#!/bin/bash

apt-get update && \
    apt-get dist-upgrade -y && \
    apt-get install -y \
      git \
      mysql-client \
      net-tools \
      iputils-ping

echo "Set server name"
echo "ServerName localhost" | tee -a /etc/apache2/apache2.conf

echo "SEARCH THIS!!"
cd /usr/local/etc/php/modules && docker-php-ext-install pdo_mysql

echo "Script is finished!"
exec "apache2-foreground"