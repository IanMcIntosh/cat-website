# LAMP STACK

![MySQL](https://img.shields.io/badge/MySQL-5.7.20-blue.svg)
![PHP](https://img.shields.io/badge/PHP-7.2-green.svg)

## Docker Containers

The **docker-compose.yml** file will create 3 containers:

* MySQL 5.7.20
* PHP 7.2
* PHPMyAdmin 4.7.7

## Project Folder

**www** folder

This is the project folder and contains your whole php project. If this does not exist in the folder where your docker file is, it will be created. From there you will need to empty it and load your own project.

**sample** folder

The sample folder is a folder with an express sample site. Before running the `docker-compose` command rename it to `www` and the site will run saying Hello World when you visit the site on `http://localhost:8000` 

**configs** folder

This folder is for the scripts that run while your container is being created. This includes your ini files and modules that are loaded during the setup.

## Run an existing project

Create a project folder where you keep your project.

Run the following command in the terminal to get the working area setup

`curl -LOk https://github.com/bcsjk11/LAMP-Docker-Setup/archive/master.zip`

Unzip and then remove the zip file 

`unzip master.zip && rm master.zip`

Create a folder called www and put load your project in there (probably using git)

**OPTIONAL** If you want to run the sample project, rename the sample directory to www now.

Now run the **docker-compose.yml** file, this may take a minute - depending on wether you have the container images downloaded

`docker-compose up -d`

The containers are now up and running and you will need to load the SQL file to set up your database

The following configs are default in the compose file:

* port for the node project : 8000 (visit site on http://localhost:8000)
* port for phpmyadmin: 8080 (visit site on http://localhost:8080)

If you want to edit the MySQL using a client app use port localhost:3306 to access it and use the credentials set in the docker file.